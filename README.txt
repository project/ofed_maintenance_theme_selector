This module is inspired by the "Maintenance theme" project posted by Howell H.

Author page:  http://drupal.org/user/316595
Project page: http://drupal.org/project/maintenance_theme

The initial module was developed for the version 6 of Drupal but now we need to
use the similar functionnalities with the Drupal 7. This module allow to set an
another theme to display the maintenance page.

This module was initially created by Matthieu Roy
(http://drupal.org/user/2340844).

How to install.
  1 - Download and extract the archive.
  2 - Move or copy this inside your modules folder
    (for example: sites/all/modules).
  3 - Go to the modules page and enable this module.
  4 - Go to Site Maintenance page (admin/config/development/maintenance) choose
    which theme the maintenance page should display in and Save configuration.

How to use.
  1 - Choose which theme the maintenance page should display in the
    Site Maintenance page (admin/config/development/maintenance).
  2 - Put your site into the maintenance mode.
  3 - Log out and look your site in maintenance mode.
